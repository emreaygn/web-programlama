﻿using LetsHelp.DAL;
using LetsHelp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LetsHelp.Controllers
{
    public class KullaniciController : Controller
    {
        private LetsHelpContext db = new LetsHelpContext();

         public ActionResult Index()
        {
            if (Session["UserId"] != null)
            {
                int Id = (int)Int32.Parse(Session["UserId"].ToString());
                var user = db.Uyeler.Where(u => u.UyeID == Id).FirstOrDefault();
                return View(user);
            }
            else
            {
                return View("Giris");
            }
        }

        public ActionResult Kayit()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Kayit([Bind(Include = "UyeID,Ad,Soyad,Email,Sifre,SifreDogrula")]Uye uye)
        {
            if(ModelState.IsValid)
            {
                
                var varMi = emailMevcutMu(uye.Email);
                if (varMi)
                {
                    ModelState.AddModelError("EmailExist", "Girdiğiniz email kullanılmaktadır");
                    return View(uye);
                }

                else
                {
                    db.Uyeler.Add(uye);
                    db.SaveChanges();
                    return RedirectToAction("Giris", "Kullanici");
                }

            }
            return View(uye);
        }

        public ActionResult Giris()
        {
            
            if (Session["UserId"] != null)
            {
                int Id = (int)Int32.Parse(Session["UserId"].ToString());
                var user = db.Uyeler.Where(u => u.UyeID == Id).FirstOrDefault();
                return View("Index", user);
            }

            return View();
        }

        [HttpPost]
        public ActionResult Giris(Uye uye)
        {
            var user = db.Uyeler.Where(u => u.Email == uye.Email && u.Sifre == uye.Sifre).FirstOrDefault();
            if (user != null)
            {
                Session["UserId"] = user.UyeID;
                if(user.Admin == true)
                {
                    Session["Admin"] = user.Ad;
                }
                return View("Index", user);

            }

            ViewBag.Error = "Yanlış kullanıcı adı/şifre kombinasyonu, tekrar deneyiniz";
            return View();
        }

        public ActionResult Cikis()
        {
            Session["Admin"] = null;
            Session["UserId"] = null;
            return RedirectToAction("Kampanya", "Home");
        }

        [NonAction]
        public bool emailMevcutMu(string email)
        {
            var x = db.Uyeler.Where(a => a.Email == email).FirstOrDefault();
            return x != null;
        }

    }
}