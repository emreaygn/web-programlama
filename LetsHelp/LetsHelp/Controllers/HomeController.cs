﻿using LetsHelp.DAL;
using LetsHelp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LetsHelp.Controllers
{
    public class HomeController : Controller
    {
        private LetsHelpContext db = new LetsHelpContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Kampanya()
        {
            List<Kampanya> KampanyalarSonEklenen = (db.Kampanyalar
                .OrderByDescending(x => x.YaziTarihi)
                .ToList());

            KampanyaViewModel finalItem = new KampanyaViewModel();

            finalItem.KampanyalarSonEklenen = KampanyalarSonEklenen;


            return View(finalItem);
        }
    }
}