﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LetsHelp.DAL;
using LetsHelp.Models;

namespace LetsHelp.Controllers
{
    public class KampanyaController : Controller
    {
        private LetsHelpContext db = new LetsHelpContext();

        // GET: Kampanya
        [Filters.AdminYetki]
        public ActionResult Index()
        {
            return View(db.Kampanyalar.ToList());
        }
        

        // GET: Kampanya/Create
        [Filters.KullaniciYetki]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kampanya/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KampanyaID,Baslik,YaziTarihi,YaziMetni")] Kampanya kampanya)
        {
            if (ModelState.IsValid)
            {
                kampanya.YaziTarihi = DateTime.Now;
                db.Kampanyalar.Add(kampanya);
                db.SaveChanges();
                return RedirectToAction("Kampanya", "Home");
            }

            return View(kampanya);
        }

        // GET: Kampanya/Edit/5
        [Filters.AdminYetki]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kampanya kampanya = db.Kampanyalar.Find(id);
            if (kampanya == null)
            {
                return HttpNotFound();
            }
            return View(kampanya);
        }

        // POST: Kampanya/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KampanyaID,Baslik,YaziTarihi,YaziMetni")] Kampanya kampanya)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kampanya).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kampanya);
        }

        // GET: Kampanya/Delete/5
        [Filters.AdminYetki]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kampanya kampanya = db.Kampanyalar.Find(id);
            if (kampanya == null)
            {
                return HttpNotFound();
            }
            return View(kampanya);
        }

        // POST: Kampanya/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kampanya kampanya = db.Kampanyalar.Find(id);
            db.Kampanyalar.Remove(kampanya);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
