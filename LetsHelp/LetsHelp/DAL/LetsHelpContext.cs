﻿using LetsHelp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace LetsHelp.DAL
{
    public class LetsHelpContext : DbContext
    {
        public LetsHelpContext() : base("LetsHelp")
        {

        }

        public DbSet<Uye> Uyeler { get; set; }

        public DbSet<Kampanya> Kampanyalar { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        
    }
}