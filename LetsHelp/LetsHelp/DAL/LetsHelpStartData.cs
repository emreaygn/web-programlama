﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LetsHelp.Models;

namespace LetsHelp.DAL
{
    public class LetsHelpStartData : System.Data.Entity.DropCreateDatabaseIfModelChanges<LetsHelpContext>
    {

        protected override void Seed(LetsHelpContext context)
        {
            var kampanyalar = new List<Kampanya>
            {
                new Kampanya{Baslik="Kanserle Mücedele", YaziMetni="Kansere karşı başlattığımız mücadelemizde bize destek olun.",YaziTarihi=DateTime.Parse("2018-12-21") },
                new Kampanya{Baslik="Darüşşafaka", YaziMetni="Kimsesiz çocuklara yardım kampanyamızda sizde yanımızda olun.",YaziTarihi=DateTime.Parse("2018-12-21") },
                new Kampanya{Baslik="Yeşil Ay", YaziMetni="Bağımlılığa karşı mücadelemizde yanımızda durun.",YaziTarihi=DateTime.Parse("2018-12-21") }

            };
            kampanyalar.ForEach(s => context.Kampanyalar.Add(s));
            context.SaveChanges();

            var uye = new List<Uye>
            {
                new Uye{Ad="Emre",Soyad="Aygün",Email="emreaygn4@gmail.com",Admin=true,Sifre="123456",SifreDogrula="123456"},
                new Uye{Ad="Mert",Soyad="Çelik",Email="mert@gmail.com",Admin=false,Sifre="123456",SifreDogrula="123456"}

            };
            uye.ForEach(s => context.Uyeler.Add(s));
            context.SaveChanges();
        }

    }
}