﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LetsHelp.Startup))]
namespace LetsHelp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
