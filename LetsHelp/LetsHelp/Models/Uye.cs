﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LetsHelp.Models
{
    public class Uye
    {

        public int UyeID { get; set; }

        public string Ad { get; set; }

        public string Soyad { get; set; }

        public bool Admin { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Email alani bos birakilamaz")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Sifre alani bos birakilamaz")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Şifre en az 6 karakter uzunluğunda olmalıdır")]
        public string Sifre { get; set; }

        [NotMapped]
        [DataType(DataType.Password)]
        [Compare("Sifre", ErrorMessage = "Girdiğiniz şifreler uyuşmamakta")]
        public string SifreDogrula { get; set; }

        public ICollection<Kampanya> Kampanyalar { get; set; }
    }
}