﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LetsHelp.Models
{
    public class KampanyaViewModel
    {
        public List<Kampanya> Kampanyalar { get; set; }
        public List<Kampanya> KampanyalarSonEklenen { get; internal set; }
    }
}