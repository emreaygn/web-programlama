﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LetsHelp.Models
{
    public class Kampanya
    {
        public int KampanyaID { get; set; }

        [Required]
        [Display(Name = "Başlık")]
        public string Baslik { get; set; }

        [Display(Name = "Yazı Tarihi")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime  YaziTarihi { get; set; }

        [Required]
        [Display(Name = "Yazı Metni")]
        public string YaziMetni { get; set; }
    }
}